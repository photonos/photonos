# PhotonOS Main Repository

PhotonOS is a hybrid, cross-platform, Linux-based operating system including support for Linux and Android applications, all with a touch-friendly user interface

## Contributing

Submit issues either under this main repository or under another repo in the PhotonOS GitLab group, and a developer will try and respond. Contributions in the form of Pull Requests (please check them before submitting) are always welcome.

## License

PhotonOS is licensed by The PhotonOS Project under the [Apache 2.0](LICENSE) license

PhotonUI is closed-source and not licensed for commercial or indiviudal reuse

LineageOS sources are licensed by The LineageOS Project under the [Apache 2.0](LICENSE) license

Maru OS sources are licensed by The Maru OS Project under the [Apache 2.0](LICENSE) license
